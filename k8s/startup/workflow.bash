kubectl create ns tst-dev
kubectl config set-context --current --namespace tst-dev
kubectl create secret generic regcred --from-file=.dockerconfigjson=/home/juno/.docker/config.json --type=kubernetes.io/dockerconfigjson -n gitlab-agent

kubectl apply -f startup/ukrik.autos/cert-ukrik-autos.yml
kubectl get event